#!/bin/bash

source /i9mlib-6.0.sh

chown -R jovyan:users /home/jovyan/.jupyter

sudo PATH=$PATH -Eu jovyan /usr/local/bin/start-notebook.py $@ &

pid="$!"
waitPids $pid