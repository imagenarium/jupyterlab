<@requirement.NODE ref='jupyterlab' primary='jupyterlab-${namespace}' />

<@requirement.PARAM name='PUBLISHED_PORT' value='8888' type='port' />

<@img.TASK 'jupyterlab-${namespace}' 'imagenarium/jupyterlab:4.2.5'>
  <@img.NODE_REF 'jupyterlab' />
  <@img.VOLUME '/home/jovyan' />
  <@img.PORT PARAMS.PUBLISHED_PORT '8888' />
  <@img.CHECK_PORT '8888' />
</@img.TASK>